Function conani20
{
    conan install . -s build_type=Debug -s compiler.libcxx=libc++ -s cppstd=20 --install-folder=cmake-build-debug --build missing
}

Set-Alias -Name conan-i20 -Value conani20