/*
 * Game.cpp
 *
 *  Created on: 25.10.2016
 *      Author: mariocake
 */

#include "Game.hpp"

Game::Game():Game(1) {}

Game::Game(int tickRate):tickRate(tickRate){
	creaturesTotal = 1024;
	renderSlowdown = 1;
	currentSnakeNum = 0;
	if(snakes.empty()){
		for(int i = 0; i < creaturesTotal; i++){
			Snake temp;
			snakes.push_back(temp);
			snakes.back().init();

 		}
	}
	currentSnake = &snakes[0];
}


Game::~Game() {
	//delete currentSnake;
}
void Game::update(){
	this->tick(tickRate);
	SDL_Delay(renderSlowdown-1);
	this->render();
	if(!currentSnake->alive || currentSnake->done){
		nextSnake();
	}
}
void Game::tick(int amount){
	//std::cout << currentSnake->fitness << std::endl;

	for(int i = 0; i < amount; i++){
		currentSnake->update();
	}
}
void Game::render(){
	//std::cout << "Render" << std::endl;
	SDL_RenderClear(Window::renderer);
	currentSnake->render();
	SDL_RenderPresent(Window::renderer);
}

bool Game::sort(Snake& a, Snake& b){
	return a.fitness < b.fitness;
}

void Game::nextSnake(){
	currentSnakeNum++;
	if(currentSnakeNum < creaturesTotal){
		currentSnake = &snakes[currentSnakeNum];
		Playground::applesEaten = 0;
		currentSnake->playground.moveApple();
	}else{
		clock_t begin = clock();
		std::sort(snakes.begin(), snakes.end(), Game::sort); // TODO: Make Sort faster!
		std::cout << clock()-begin << std::endl;
		begin = clock();
		std::cout << "Best Fitness: " << snakes.back().fitness << std::endl;
		std::cout << "Worst Fitness: " << snakes.front().fitness << std::endl;
		int fullFitness = 0;
		for(std::vector<Snake>::iterator snake = snakes.begin(); snake != snakes.end(); ++snake){
			fullFitness += (*snake).fitness;
			//(*snake).brain.remakeMesh();
			(*snake).alive = true;
			(*snake).done = false;
			(*snake).fitness = 0;
			(*snake).ticked = 0;
			(*snake).turnDir = 0;
		}
		std::cout << "Avarage Fitness: " << (float)fullFitness / snakes.size() << std::endl;
		std::cout << clock()-begin << std::endl;
		//std::cout << "done sorting" << std::endl;

		/*int counter = 0;
		for(std::vector<Snake>::iterator snake = snakes.begin(); snake != snakes.end(); ++snake, ++counter){
			std::cout << (*snake).fitness << ", ";
			if(counter >= 50){
				std::cout << std::endl;
				counter = 0;
			}
		}*/
		std::cout << "start looking" << std::endl;
		std::cout << "DELAY" << std::endl;
		this->kill();
		this->reproduce();
		std::cout << "Stop looking" << std::endl;
		currentSnakeNum = 0;
		currentSnake = &snakes[currentSnakeNum];
		//std::cout << (*currentSnake).brain.inputNodes->size() << std::endl;
		//std::cout << currentSnake << std::endl;
	}
}

void Game::kill(){
	int killed = 0;
	int numOfAll = snakes.size();
	//std::cout << numOfAll << std::endl;
	std::cout << "Should make smaller" << std::endl;
	for(std::vector<Snake>::iterator snake = snakes.begin(); killed < (numOfAll/2); ++snake){
		if(snake == snakes.end()-1){
			std::cout << "Woops" << std::endl << std::endl << std::endl;
			snake = snakes.begin()+killed;
		}
		if((rand()%70) <= (((numOfAll/(float)numOfAll+killed))*100)){
			//std::cout << "Kill: " <<  killed << std::endl;
			killed++;
			std::iter_swap(snake, snakes.end()-1);
			//std::cout << "Erasing!: " << killed << std::endl;
			snakes.erase(snakes.end()-1);
			//snakes.erase(snake);
		}
	}
	std::cout << "Should be smaller" << std::endl;
	/*int counter = 0;
	for(std::vector<Snake>::iterator snake = snakes.begin(); snake != snakes.end(); ++snake, ++counter){
		std::cout << (*snake).fitness << ", ";
		if(counter >= 50){
			std::cout << std::endl;
			counter = 0;
		}
	}*/
	//snakes.erase(snakes.begin(), snakes.begin()+(creaturesTotal/2));

	//std::cout << snakes.size() << std::endl;
	//std::cout << "Best Fitness: " << snakes.back().fitness << std::endl;
	//std::cout << "Worst Fitness: " << snakes.front().fitness << std::endl;
	std::cout << "Killed all!" << std::endl;
}


void Game::reproduce(){
	int counter = 0;

    std::cout << "Create new snakes" << std::endl;
	for(std::vector<Snake>::iterator snake = snakes.begin(); counter < (creaturesTotal/2); ++snake, counter++){
		//std::cout << "Counter: " << counter <<  std::endl;
		Snake tmp;
		snakes.push_back(tmp);
		snakes.back().init(true);
		snakes.back().brain.clone((*snake).brain);
	}
    std::cout << "Created new snakes" << std::endl;

	std::cout << snakes.capacity() << " : " << snakes.size() << std::endl;
	/*for(std::vector<Snake>::iterator snake = snakes.begin(); snake != snakes.end(); ++snake){
		(*snake).brain.remakeMesh();
	}*/
}
