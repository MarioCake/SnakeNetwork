/*
 * Game.hpp
 *
 *  Created on: 25.10.2016
 *      Author: mariocake
 */

#ifndef GAME_HPP_
#define GAME_HPP_

#include <vector>
#include <algorithm>

#include "SnakeLogic/Playground.hpp"
#include "ScreenHandler/WindowHandler.hpp"
#include "Snake.hpp"

class Game {
public:
	Window window;
	int tickRate;
	std::vector<Snake> snakes;
	Snake* currentSnake;
	int creaturesTotal;
	float renderSlowdown;
	Game();
	Game(int tickRate);
	int currentSnakeNum;
	virtual ~Game();
	void tick(int amount);
	void init();
	void render();
	void update();
	void nextSnake();
	void kill();
	void reproduce();
private:
	static bool sort(Snake& a, Snake& b);
};

#endif /* GAME_HPP_ */
