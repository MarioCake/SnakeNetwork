/*
 * Snake.cpp
 *
 *  Created on: 29.10.2016
 *      Author: mariocake
 */

#include "Snake.hpp"


Snake::Snake() {
	this->turnDir = 0;
	this->alive = true;
	this->fitness = 0;
	this->done = false;
	this->ticked = 0;
}

Snake::Snake(const Snake& other){ // copy constructor
	//std::cout << "copy const" << std::endl;
	this->brain = other.brain;
	this->turnDir = 0;
	this->alive = true;
	this->done = false;
	this->fitness = other.fitness;
	Playground::applesEaten = 0;
	this->playground.moveApple();
	this->ticked = 0;
}

Snake::Snake(Snake&& other)noexcept{ // move constructor
	//std::cout << "move const" << std::endl;
	this->brain = std::move(other.brain);
	this->turnDir = 0;
	this->alive = true;
	this->done = false;
	this->fitness = other.fitness;
	Playground::applesEaten = 0;
	this->playground.moveApple();
	this->ticked = 0;
}

Snake& Snake::operator=(Snake&& other)noexcept{ // move assignment
	//std::cout << "move ass" << std::endl;
	this->brain = std::move(other.brain);
	this->turnDir = 0;
	this->alive = true;
	this->done = false;
	this->fitness = other.fitness;
	Playground::applesEaten = 0;
	this->playground.moveApple();
	this->ticked = 0;

	return *this;
}

Snake& Snake::operator=(const Snake& other){ // copy assignment
	std::cout << "copyy ass" << std::endl;
	this->brain = other.brain;
	this->turnDir = 0;
	this->alive = true;
	this->done = false;
	this->fitness = other.fitness;
	Playground::applesEaten = 0;
	this->playground.moveApple();
	this->ticked = 0;
	return *this;
}

Snake::~Snake(){
	//std::cout << "woop" << std::endl;
}

void Snake::calcTurnDir(){
	WorkingNeuron highestOutput = (*(brain.outputNodes))[0];
	for(std::vector<WorkingNeuron>::iterator node = (brain.outputNodes->begin()+1); node != brain.outputNodes->end(); ++node){
		if((*node).getValue() > highestOutput.getValue()){
			highestOutput = (*node);
		}
	}
	if(highestOutput.name == "up")turnDir = 1;
	else if (highestOutput.name == "left") turnDir = 2;
	else if (highestOutput.name == "down") turnDir = 3;
	else if (highestOutput.name == "right") turnDir = 0;
	else turnDir = 4;
}
void Snake::init(bool newMesh){
	brain.addInputNode("posX");
	brain.addInputNode("posY");
	brain.addInputNode("aPosX");
	brain.addInputNode("aPosY");
	brain.addInputNode("previousDirection");
	brain.addInputNode("time");

    for(int x = 0; x < playground.fieldX; ++x){
    	for(int y = 0; y < playground.fieldY; ++y){
    		brain.addInputNode(std::to_string(x) + "-" + std::to_string(y));
    	}
    }

	brain.addOutputNode("up");
	brain.addOutputNode("right");
	brain.addOutputNode("left");
	brain.addOutputNode("down");

	brain.createHiddens(50);

    if (newMesh) {
        brain.generateNewMesh();
    }
}

void Snake::update(){
	if(!this->done){
		brain.getInputNodeByName("posX")->setInputValue(playground.bodyParts[0].posX);
		brain.getInputNodeByName("posY")->setInputValue(playground.bodyParts[0].posY);
		brain.getInputNodeByName("aPosX")->setInputValue(playground.apple.posX);
		brain.getInputNodeByName("aPosY")->setInputValue(playground.apple.posY);
		brain.getInputNodeByName("previousDirection")->setInputValue(turnDir);
		brain.getInputNodeByName("time")->setInputValue(ticked);

		std::string tile;

        for(int x = 0; x < playground.fieldX; ++x){
            for(int y = 0; y < playground.fieldY; ++y){
                tile = playground.getTileNameAtPos(x, y);
                brain.getInputNodeByName(std::to_string(x) + "-" + std::to_string(y))->setInputValue((tile == "null" ? 0 : (tile == "apple" ? 1 : -1)));
            }
        }

		brain.calcOutputs();
		calcTurnDir();
		playground.turnSnake(turnDir);
		playground.moveSnake();
		if(playground.ateApple()){
			this->fitness += playground.bodyParts.size()*5;
			playground.moveApple();
		}
		if(playground.colliding()){
			this->alive = false;
		}else{
			this->fitness += playground.bodyParts.size();
		}
		ticked++;
		if(ticked > 500 || !alive){
			this->done = true;
			this->playground.bodyParts.erase(this->playground.bodyParts.begin(), this->playground.bodyParts.end());
			this->playground.createNewSnake(8, 4);
		}

	}
	//std::cout << "Print" << std::endl;
}

void Snake::render(){
	//std::cout << "Render 2" << std::endl;
	playground.render();
}


