/*
 * Snake.hpp
 *
 *  Created on: 29.10.2016
 *      Author: mariocake
 */

#ifndef SNAKE_HPP_
#define SNAKE_HPP_

#include "SnakeLogic/Playground.hpp"
#include "NeuronalNetwork/Network.hpp"
#include <iostream>
#include <utility>

class Snake {
public:
	Network brain;
	Playground playground;
	bool alive;
	bool done;
	int fitness;
	int ticked;
	int turnDir;
	void calcTurnDir();
	void update();
	void render();
	void init(bool newMash = true);
	Snake();
	Snake(Snake&& other)noexcept;
	Snake& operator=(Snake&& other)noexcept;
	Snake& operator=(const Snake& other);
	Snake(const Snake& other);
	virtual ~Snake();
};

#endif /* SNAKE_HPP_ */
