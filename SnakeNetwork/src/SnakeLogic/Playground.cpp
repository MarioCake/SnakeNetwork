/*
 * Playground.cpp
 *
 *  Created on: 24.10.2016
 *      Author: mariocake
 */

#include "Playground.hpp"

std::vector<Snaject> Playground::tiles;
Snaject Playground::apple;
unsigned short int Playground::applesEaten = 0;
int Playground::startX = -1;
int Playground::startY = -1;

Playground::Playground() {
	Playground::applesEaten = 0;
	this->fieldX = 16;
	this->fieldY = 9;
	this->tileSize = 75;
	this->makeSnakeBigger = false;
	if(Playground::tiles.empty()){
		for(int i = 0; i < this->fieldX; i++){
			for(int j = 0; j < this->fieldY; j++){
				if((rand()%10) == 5){
					Playground::tiles.push_back(Snaject(i-1, j-1, 0));
				}
			}
		}
		for(std::vector<Snaject>::iterator tile = Playground::tiles.begin(); tile != Playground::tiles.end(); ++tile){
			(*tile).setName("stone");
		}
		Playground::apple.setName("apple");
		Playground::moveApple();
		Playground::startX = Playground::apple.posX;
		Playground::startY = Playground::apple.posY;
		//Playground::cords.push_back(AppleCords(this->apple.posX, this->apple.posY));
	}
	createNewSnake(8, 4);
}

Playground::~Playground() noexcept{
	bodyParts.erase(bodyParts.begin(), bodyParts.end());
}

std::string Playground::getTileNameAtPos(int x, int y){
	if(apple.posX == x && apple.posY == y) return apple.objectName;

	for(std::vector<Snaject>::iterator tile = tiles.begin(); tile != tiles.end(); ++tile){
		if((*tile).posX == x && (*tile).posY == y){
			return (*tile).objectName;
		}
	}
	for(std::vector<Snaject>::iterator body = bodyParts.begin()+1; body != bodyParts.end(); ++body){
		if((*body).posX == x && (*body).posY == y){
			return (*body).objectName;
		}
	}
	return "null";
}

void Playground::render(){

	apple.render(apple.posX*this->tileSize, apple.posY*this->tileSize);
	for(std::vector<Snaject>::iterator i = this->tiles.begin(); i != this->tiles.end(); ++i){
		(*i).render((*i).posX*this->tileSize, (*i).posY*this->tileSize);
	}
	for(std::vector<Snaject>::iterator i = this->bodyParts.begin(); i != this->bodyParts.end(); ++i){
		(*i).render((*i).posX*this->tileSize, (*i).posY*this->tileSize);
	}

}

bool Playground::colliding(){
	if(bodyParts[0].posX > this->fieldX-1){
		return true;
	}
	if(bodyParts[0].posX < 0){
			return true;
	}
	if(bodyParts[0].posY < 0){
			return true;
	}
	if(bodyParts[0].posY > this->fieldY-1){
			return true;
	}

	for(std::vector<Snaject>::iterator i = this->tiles.begin(); i != this->tiles.end(); ++i){
		if((*(i)) == (*(bodyParts.begin()))){
			return true;
		}
	}
	for(std::vector<Snaject>::iterator i = this->bodyParts.begin(); i != this->bodyParts.end(); ++i){
		if((*(i) == *(bodyParts.begin()) && (i != bodyParts.begin()))){
			return true;
		}
	}
	return false;
}
bool Playground::ateApple(){
	if((*bodyParts.begin()) == apple){
		this->applesEaten++;
		this->makeSnakeBigger = true;
		//std::cout << "Bigger" << std::endl;
		return true;
	}
	return false;
}
void Playground::moveApple(){
	bool appleTriedToMove = false;
	bool colliding = false;
	do{
		colliding = false;
		if(Playground::startX == -1 || appleTriedToMove){
			Playground::apple.posX = rand()%this->fieldX;
			Playground::apple.posY = rand()%this->fieldY;
			if(appleTriedToMove){
				--applesEaten;
			}
		}else{
			Playground::apple.posX = Playground::startX;
			Playground::apple.posY = Playground::startY;
			appleTriedToMove = true;
		}
		for(std::vector<Snaject>::iterator i = Playground::tiles.begin(); i != Playground::tiles.end() && !colliding; ++i){
			if((*(i)) == (apple)){
				colliding = true;
			}
		}
		for(std::vector<Snaject>::iterator i = this->bodyParts.begin(); i != this->bodyParts.end() && !colliding; ++i){
			if((*(i)) == (apple)){
				colliding = true;
			}
		}

		if(!colliding && ((apple.posX == this->fieldX-1 || apple.posX == 0) && (apple.posY == this->fieldY-1 || apple.posY == 0))){
			if((getTileNameAtPos(apple.posX-1, apple.posY) == "stone") || (getTileNameAtPos(apple.posX, apple.posY-1) == "stone")){
				colliding = true;
			}else if((getTileNameAtPos(apple.posX+1, apple.posY) == "stone") || (getTileNameAtPos(apple.posX, apple.posY+1) == "stone")){
				colliding = true;
			}
		}

	}while(colliding);
}
void Playground::addBodyPart(){
	bodyParts.push_back(bodyParts.back());
}
void Playground::createNewSnake(int posX, int posY){
	if(!bodyParts.empty()){
		bodyParts.erase(bodyParts.begin(), bodyParts.end()-1);
		//std::cout << bodyParts.size() << std::endl;
		bodyParts.back().posX = posX;
		bodyParts.back().posY = posY;
		this->makeSnakeBigger = false;
	}else{
		bodyParts.push_back(Snaject(posX, posY, 0));
		bodyParts.back().setName("snake");
	}
}

void Playground::moveSnake(){
	if(makeSnakeBigger){
		this->addBodyPart();
		makeSnakeBigger = false;
	}
	for(std::vector<Snaject>::reverse_iterator part = bodyParts.rbegin(); part != bodyParts.rend(); ++part){
		if(part == (bodyParts.rend()-1)){
			switch(bodyParts[0].direction){
			case 0:
				(*part).posX += 1;
				break;
			case 1:
				(*part).posY -= 1;
				break;
			case 2:
				(*part).posX -= 1;
				break;
			case 3:
				(*part).posY += 1;
				break;
			default:
				std::cerr << "Wrong direction: " << bodyParts[0].direction;
			}
		}
		else *part = (*(part+1));
	}
}

void Playground::turnSnake(int turnDir){
	if(abs(turnDir - bodyParts[0].direction) == 1 || abs(turnDir - bodyParts[0].direction) == 3){
		bodyParts[0].direction = turnDir;
	}
}

float Playground::calcTileSet(){
	float highest = (float)pow(1.1, (double)this->fieldX*this->fieldY);
	float answer = 0;
	for(std::vector<Snaject>::iterator tile = Playground::tiles.begin(); tile != Playground::tiles.end(); ++tile){
		answer += (float)pow(1.1, (double)(((*tile).posX+1) + ((*tile).posY) * this->fieldX));
	}
	for(std::vector<Snaject>::iterator part = bodyParts.begin()+1; part != bodyParts.end(); ++part){
		answer += (float)pow(1.1, (double)(((*part).posX+1) + ((*part).posY) * this->fieldX));
	}
	return answer/highest;
}

