/*
 * Snaject.hpp
 *
 *  Created on: 24.10.2016
 *      Author: mariocake
 */

#ifndef SNAJECT_HPP_
#define SNAJECT_HPP_

#include <string>
#include <SDL2/SDL.h>

#include "../ScreenHandler/EasyTexture.hpp"

class Snaject {
public:
	Snaject();
	int posX;
	int posY;
	int direction; // 0-3. 0 is right, 1 is up, 2 is left, 3 is down.
	std::string objectName;
	EasyTexture texture;
	Snaject(int posX, int posY, int dir);
	Snaject(const Snaject &part);
	Snaject(Snaject&& part) noexcept;
	virtual ~Snaject() noexcept;
	bool operator==(const Snaject &part);
	Snaject& operator=(const Snaject &part);
	Snaject& operator=(Snaject&& part) noexcept;
	void setName(std::string name);
	void render(int x, int y);
private:
};

#endif /* SNAJECT_HPP_ */
