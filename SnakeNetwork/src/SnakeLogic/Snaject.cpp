/*
 * Snaject.cpp
 *
 *  Created on: 24.10.2016
 *      Author: mariocake
 */

#include "Snaject.hpp"

Snaject::Snaject() {
	this->posX = 0;
	this->posY = 0;
	this->direction = 0;
	this->objectName = "null";
}
Snaject::Snaject(int posX, int posY, int dir):posX(posX), posY(posY), direction(dir){
	this->setName("stone");
}
Snaject::~Snaject() noexcept{
	//std::cout << "Snaject destruct" << std::endl;
	//this->texture.texture = NULL;
	//this->texture.free();
}

bool Snaject::operator==(const Snaject &part){
	return ((part.posX == this->posX) && (part.posY == this->posY));
}
Snaject& Snaject::operator=(const Snaject &part){
	Snaject tmp(part);
	(*this) = std::move(tmp);
	return *this;
}

Snaject& Snaject::operator=(Snaject&& part) noexcept{
	this->posX = part.posX;
	this->posY = part.posY;
	this->direction = part.direction;
	this->texture = std::move(part.texture);
	return *this;
}

Snaject::Snaject(const Snaject &part){
	this->direction = part.direction;
	this->setName(part.objectName);
	this->posX = part.posX;
	this->posY = part.posY;
}

Snaject::Snaject(Snaject&& part) noexcept{
	this->direction = part.direction;
	this->objectName = part.objectName;
	this->posX = part.posX;
	this->posY = part.posY;
	this->texture = std::move(part.texture);
}

void Snaject::setName(std::string name){
	this->objectName = name;
	this->texture.loadFromFile("res/" + name + ".png");
}
void Snaject::render(int x, int y){
	this->texture.render(x, y);
}

