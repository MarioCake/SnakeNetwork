/*
 * Playground.hpp
 *
 *  Created on: 24.10.2016
 *      Author: mariocake
 */

#ifndef PLAYGROUND_HPP_
#define PLAYGROUND_HPP_

#include "Snaject.hpp"
#include <iostream>
#include <vector>

class Playground {
public:
	Playground();
	virtual ~Playground() noexcept;
	int tileSize;
	static std::vector<Snaject> tiles;
	std::vector<Snaject> bodyParts;
	static Snaject apple;
	int fieldX;
	int fieldY;
	static unsigned short int applesEaten;
	bool makeSnakeBigger;
	void addBodyPart();
	void createNewSnake(int posX, int posY);
	void turnSnake(int turnDir);
	float calcTileSet();
	void moveSnake();
	bool ateApple();
	void moveApple();
	void render();
	bool colliding();
	std::string getTileNameAtPos(int x, int y);
private:
	static int startX;
	static int startY;
};

#endif /* PLAYGROUND_HPP_ */
