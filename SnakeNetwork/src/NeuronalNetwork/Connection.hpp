/*
 * Connection.hpp
 *
 *  Created on: 25.10.2016
 *      Author: mariocake
 */

#ifndef CONNECTION_HPP_
#define CONNECTION_HPP_

#include <iostream>
#include <cstdlib>
#include <memory>

#include "WorkingNeuron.hpp"
#include "InputNeuron.hpp"
#include "Neuron.hpp"

class Connection {
public:
	Neuron* inputNode;
	WorkingNeuron* outputNode;
	double modifier;
	Connection();
	Connection(Connection&& connection);
	Connection(const Connection& connection);
	Connection(Neuron& input, WorkingNeuron& output);
	void setWeight(double num);
	void generateWeight();
	virtual ~Connection();
	void calcValue();
	Connection& operator =(const Connection& connection);
	Connection& operator =(Connection&& connection);
};

#endif /* CONNECTION_HPP_ */
