/*
 * WorkingNeuron.hpp
 *
 *  Created on: 29.10.2016
 *      Author: mariocake
 */

#ifndef WORKINGNEURON_HPP_
#define WORKINGNEURON_HPP_

#include "Neuron.hpp"

class WorkingNeuron : public Neuron{
public:
	WorkingNeuron();
	WorkingNeuron(Neuron &node);
	WorkingNeuron(std::string name);
	virtual ~WorkingNeuron();
	void calcOutput();
	std::string getType();
};

#endif /* WORKINGNEURON_HPP_ */
