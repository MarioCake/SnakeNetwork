/*
 * Neuron.hpp
 *
 *  Created on: 25.10.2016
 *      Author: mariocake
 */

#ifndef NEURON_HPP_
#define NEURON_HPP_

#include <iostream>
#include <vector>
#include <math.h>

class Neuron {
public:
	float input;
	float output;
	std::string name;
	Neuron();
	Neuron(std::string name);
	virtual ~Neuron();
	static float sigmoid(float value);
	void addInputValue(float value);
	void setInputValue(float value);
	virtual void calcOutput();
	float getValue();
	void setName(std::string name);
	virtual std::string getType();
};

#endif /* NEURON_HPP_ */
