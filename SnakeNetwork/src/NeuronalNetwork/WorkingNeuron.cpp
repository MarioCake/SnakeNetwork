/*
 * WorkingNeuron.cpp
 *
 *  Created on: 29.10.2016
 *      Author: mariocake
 */

#include "WorkingNeuron.hpp"

WorkingNeuron::WorkingNeuron() {}
WorkingNeuron::WorkingNeuron(std::string name):Neuron(name) {}
WorkingNeuron::WorkingNeuron(Neuron & node):Neuron(node.name) {}

WorkingNeuron::~WorkingNeuron() {}

void WorkingNeuron::calcOutput(){
	this->output = Neuron::sigmoid(this->input);
}

std::string WorkingNeuron::getType(){
	return "working";
}
