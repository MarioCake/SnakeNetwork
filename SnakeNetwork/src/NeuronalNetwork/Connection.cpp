/*
 * Connection.cpp
 *
 *  Created on: 25.10.2016
 *      Author: mariocake
 */

#include "Connection.hpp"

Connection::Connection() {
	this->inputNode = NULL;
	this->outputNode = NULL;
	modifier = 0;
}

Connection::Connection(Neuron& input, WorkingNeuron& output){
	//delete this->inputNode;
	//delete this->outputNode;
	//this->inputNode = new Neuron();
	//this->outputNode = new WorkingNeuron();

	if(input.getType() == "input"){
		this->inputNode = dynamic_cast<InputNeuron*>(&input);
	}else {
		this->inputNode = dynamic_cast<WorkingNeuron*>(&input);
	}
	this->outputNode = &output;
	this->modifier = 0;
}

Connection::~Connection() noexcept{
	//if(this->inputNode != NULL) delete this->inputNode;
	//if(this->outputNode != NULL) delete this->outputNode;
}

void Connection::setWeight(double num){
	this->modifier = num;
}

void Connection::generateWeight(){
	this->setWeight(((rand()%1001)-500)/(double)100);
}

void Connection::calcValue(){
	inputNode->calcOutput();
	outputNode->addInputValue(modifier * inputNode->getValue());
	outputNode->calcOutput();
}

Connection::Connection(const Connection& connection){
	//this->inputNode = new Neuron();
	//this->outputNode = new WorkingNeuron();

	this->inputNode = connection.inputNode;
	this->outputNode = connection.outputNode;
	this->modifier = connection.modifier;
}

Connection::Connection(Connection&& connection){
	this->inputNode = connection.inputNode;
	this->outputNode = connection.outputNode;
	connection.inputNode = NULL;
	connection.outputNode = NULL;
	this->modifier = connection.modifier;
}

Connection& Connection::operator =(const Connection& connection){
	Connection tmp(connection);
	(*this) = std::move(tmp);
	return (*this);
}

Connection& Connection::operator =(Connection&& connection){
	std::cout << "Move" << std::endl;
	this->inputNode = connection.inputNode;
	this->outputNode = connection.outputNode;
	this->modifier = connection.modifier;
	connection.inputNode = NULL;
	connection.outputNode = NULL;
	return (*this);
}
