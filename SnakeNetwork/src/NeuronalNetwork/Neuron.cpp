/*
 * Neuron.cpp
 *
 *  Created on: 25.10.2016
 *      Author: mariocake
 */

#include "Neuron.hpp"

Neuron::Neuron() {
	this->input = 0;
	this->output = 0;
	this->name = "NULL";
}
Neuron::Neuron(std::string name) {
	this->input = 0;
	this->output = 0;
	this->name = name;
}

Neuron::~Neuron() {}

float Neuron::sigmoid(float value){
	return exp(value)/(1+exp(value));
}

void Neuron::addInputValue(float value){
	input += value;
}
void Neuron::setInputValue(float value){
	this->input = value;
}

float Neuron::getValue(){
	return this->output;
}
void Neuron::setName(std::string name){
	this->name = name;
}

void Neuron::calcOutput(){
	this->output = this->input;
}

std::string Neuron::getType(){
	return "neuron";
}

