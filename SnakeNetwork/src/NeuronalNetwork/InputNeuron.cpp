/*
 * InputNeuron.cpp
 *
 *  Created on: 29.10.2016
 *      Author: mariocake
 */

#include "InputNeuron.hpp"

InputNeuron::InputNeuron() {}
InputNeuron::InputNeuron(std::string name):Neuron(name) {}
InputNeuron::InputNeuron(Neuron &node):Neuron(node.name) {}

InputNeuron::~InputNeuron() {}

void InputNeuron::calcOutput(){
	this->output = this->input;
}

std::string InputNeuron::getType(){
	return "input";
}

