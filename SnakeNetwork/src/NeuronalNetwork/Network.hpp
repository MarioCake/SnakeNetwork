/*
 * Network.hpp
 *
 *  Created on: 27.10.2016
 *      Author: mariocake
 */

#ifndef NETWORK_HPP_
#define NETWORK_HPP_

#include <thread>
#include <utility>
#include <memory>

#include "Neuron.hpp"
#include "InputNeuron.hpp"
#include "WorkingNeuron.hpp"
#include "Connection.hpp"

class Network {
public:
	std::unique_ptr<std::vector<InputNeuron>> inputNodes;
	std::unique_ptr<std::vector<WorkingNeuron>> hiddenNodes;
	std::unique_ptr<std::vector<WorkingNeuron>> outputNodes;
	std::vector<Connection> connectionsIH;
	std::vector<Connection> connectionsHO;
	void generateNewMesh();
	void createHiddens(int amount);
	InputNeuron* getInputNodeByName(std::string name);
	WorkingNeuron* getOutputNodeByName(std::string name);
	void addInputNode(std::string name);
	void addOutputNode(std::string name);
	void clone(Network &net);
	void remakeMesh();
	void calcOutputs();
	Network();
	virtual ~Network() noexcept;
	Network(Network&& other) noexcept; //move constructor
	Network(const Network& other); // copy constructor
	Network& operator=(const Network& other); //copy assignment
	Network& operator=(Network&& other) noexcept; //move assignment
private:
	void deleteNodes();
	void mutate(Network &net);
	void generateWeights();
	void connectIwithH();
	void connectHwithO();
};

#endif /* NETWORK_HPP_ */
