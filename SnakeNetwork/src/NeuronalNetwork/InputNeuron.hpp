/*
 * InputNeuron.hpp
 *
 *  Created on: 29.10.2016
 *      Author: mariocake
 */

#ifndef INPUTNEURON_HPP_
#define INPUTNEURON_HPP_

#include "Neuron.hpp"

class InputNeuron : public Neuron{
public:
	InputNeuron();
	InputNeuron(Neuron &node);
	InputNeuron(std::string name);
	virtual ~InputNeuron();
	void calcOutput();
	std::string getType();
};

#endif /* INPUTNEURON_HPP_ */
