/*
 * Network.cpp
 *
 *  Created on: 27.10.2016
 *      Author: mariocake
 */

#include "Network.hpp"

Network::Network() {
	this->inputNodes = std::move(std::unique_ptr<std::vector<InputNeuron>>(new std::vector<InputNeuron>));
	this->hiddenNodes = std::move(std::unique_ptr<std::vector<WorkingNeuron>>(new std::vector<WorkingNeuron>));
	this->outputNodes = std::move(std::unique_ptr<std::vector<WorkingNeuron>>(new std::vector<WorkingNeuron>));
}

Network::~Network()noexcept{
	//inputNodes->clear();
	//hiddenNodes->clear();
	//outputNodes->clear();

	this->deleteNodes();

	this->connectionsIH.erase(this->connectionsIH.begin(), this->connectionsIH.end());
	this->connectionsHO.erase(this->connectionsHO.begin(), this->connectionsHO.end());
}

void Network::deleteNodes(){
	if(this->inputNodes != NULL){
		this->inputNodes->erase(this->inputNodes->begin(), this->inputNodes->end());
		//std::vector<InputNeuron>().swap(*this->inputNodes);
		//delete this->inputNodes;
	}
	if(this->hiddenNodes != NULL){
		this->hiddenNodes->erase(this->hiddenNodes->begin(), this->hiddenNodes->end());
		//std::vector<WorkingNeuron>().swap(*this->hiddenNodes);
		//delete this->hiddenNodes;
	}
	if(this->outputNodes != NULL){
		this->outputNodes->erase(this->outputNodes->begin(), this->outputNodes->end());
		//std::vector<WorkingNeuron>().swap(*this->outputNodes);
		//delete this->outputNodes;
	}
}

void Network::generateNewMesh(){
	this->connectionsIH.erase(this->connectionsIH.begin(), this->connectionsIH.end());
	this->connectionsHO.erase(this->connectionsHO.begin(), this->connectionsHO.end());
	//connectionsIH.clear();
	//connectionsHO.clear();

    std::thread t1(&Network::connectIwithH, this);
    std::thread t2(&Network::connectHwithO, this);

    t1.join();
    t2.join();
}

void Network::createHiddens(int amount){
	for(int i = 0; i < amount; i++){
		WorkingNeuron temp;
		temp.setName("Hidden Node");
		hiddenNodes->push_back(temp);
	}
}

void Network::connectIwithH(){
	//Connect each InputNode with every HiddenNode

    connectionsIH.reserve(inputNodes->size() * hiddenNodes->size());

	for(std::vector<InputNeuron>::iterator inputNode = inputNodes->begin(); inputNode != inputNodes->end(); ++inputNode){
		for(std::vector<WorkingNeuron>::iterator hiddenNode = hiddenNodes->begin(); hiddenNode != hiddenNodes->end(); ++hiddenNode){
			connectionsIH.emplace_back((*inputNode), (*hiddenNode));
		}
	}

    for(std::vector<Connection>::iterator connection = connectionsIH.begin(); connection != connectionsIH.end(); ++connection){
        (*connection).generateWeight();
    }
}
void Network::connectHwithO(){
	//Connect each HiddenNode with every OutputNode

    connectionsHO.reserve(hiddenNodes->size() * outputNodes->size());

	for(std::vector<WorkingNeuron>::iterator hiddenNode = hiddenNodes->begin(); hiddenNode != hiddenNodes->end(); ++hiddenNode){
		for(std::vector<WorkingNeuron>::iterator outputNode = outputNodes->begin(); outputNode != outputNodes->end(); ++outputNode){
			connectionsHO.emplace_back((*hiddenNode), (*outputNode));
		}
	}

    for(std::vector<Connection>::iterator connection = connectionsHO.begin(); connection != connectionsHO.end(); ++connection){
        (*connection).generateWeight();
    }
}

void Network::clone(Network &net){
	this->deleteNodes();
	(*this->inputNodes) = (*net.inputNodes);
	(*this->hiddenNodes) = (*net.hiddenNodes);
	(*this->outputNodes) = (*net.outputNodes);

	this->mutate(net);

	this->remakeMesh();


}
InputNeuron* Network::getInputNodeByName(std::string name){
	for(std::vector<InputNeuron>::iterator _node = inputNodes->begin(); _node != inputNodes->end(); ++_node){
		if((*_node).name == name){
			return &(*_node);
		}
	}
	std::cerr << "Couldn't retrive InputNode from Name: " << name << std::endl;
	return NULL;
}

WorkingNeuron* Network::getOutputNodeByName(std::string name){
	for(std::vector<WorkingNeuron>::iterator _node = outputNodes->begin(); _node != outputNodes->end(); ++_node){
		if((*_node).name == name){
			return &(*_node);
		}
	}
	std::cerr << "Couldn't retrive OutputNode from Name: " << name << std::endl;
	return NULL;
}

void Network::mutate(Network &net){
	std::vector<Connection>().swap(connectionsIH);
	std::vector<Connection>().swap(connectionsHO);
	for(std::vector<Connection>::iterator connection = net.connectionsIH.begin(); connection != net.connectionsIH.end(); ++connection){
		if((rand()%10) == 5){
			this->connectionsIH.push_back((*connection));
			this->connectionsIH.back().setWeight((((double)(rand()%501)-250)/1000) + ((*connection).modifier));
		}else{
			this->connectionsIH.push_back((*connection));
		}
	}
	for(std::vector<Connection>::iterator connection = net.connectionsHO.begin(); connection != net.connectionsHO.end(); ++connection){
		if((rand()%10) == 5){
			this->connectionsHO.push_back((*connection));
			this->connectionsHO.back().setWeight((((double)(rand()%501)-250)/1000) + ((*connection).modifier));
		}else{
			this->connectionsHO.push_back((*connection));
		}
	}
}

void Network::calcOutputs(){
	for(std::vector<InputNeuron>::iterator node = inputNodes->begin(); node != inputNodes->end(); ++node){
		(*node).calcOutput();
	}
	for(std::vector<WorkingNeuron>::iterator node = hiddenNodes->begin(); node != hiddenNodes->end(); ++node){
		(*node).setInputValue(0);

	}
	for(std::vector<WorkingNeuron>::iterator node = outputNodes->begin(); node != outputNodes->end(); ++node){
		(*node).setInputValue(0);
	}
	for(std::vector<Connection>::iterator connection = connectionsIH.begin(); connection != connectionsIH.end(); ++connection){
		(*connection).calcValue();
	}

	for(std::vector<Connection>::iterator connection = connectionsHO.begin(); connection != connectionsHO.end(); ++connection){
		(*connection).calcValue();
	}
}

void Network::remakeMesh(){
	std::vector<double> weights;
	for(std::vector<Connection>::iterator connection = connectionsIH.begin(); connection != connectionsIH.end(); ++connection){
		weights.push_back((*connection).modifier);
	}
	std::vector<Connection>().swap(connectionsIH);
	connectIwithH();
	int counter = 0;
	for(std::vector<Connection>::iterator connection = connectionsIH.begin(); connection != connectionsIH.end(); ++connection, counter++){
		(*connection).modifier = weights[counter];
	}

	std::vector<double>().swap(weights);

	for(std::vector<Connection>::iterator connection = connectionsHO.begin(); connection != connectionsHO.end(); ++connection){
		weights.push_back((*connection).modifier);
	}
	std::vector<Connection>().swap(connectionsHO);
	connectHwithO();
	counter = 0;
	for(std::vector<Connection>::iterator connection = connectionsHO.begin(); connection != connectionsHO.end(); ++connection, counter++){
		(*connection).modifier = weights[counter];
	}

	std::vector<double>().swap(weights);
}

void Network::addInputNode(std::string name){
	(*inputNodes).push_back(std::move(InputNeuron(name)));
}
void Network::addOutputNode(std::string name){
	outputNodes->push_back(std::move(WorkingNeuron(name)));
}

Network::Network(Network&& other)noexcept{ //move constructor
	//std::cout << "Move Cons" << std::endl;
	this->inputNodes = std::move(other.inputNodes);
	this->outputNodes = std::move(other.outputNodes);
	this->hiddenNodes = std::move(other.hiddenNodes);
	this->connectionsHO = other.connectionsHO;
	this->connectionsIH = other.connectionsIH;
	std::vector<Connection>().swap(other.connectionsIH);
	std::vector<Connection>().swap(other.connectionsHO);
}

Network::Network(const Network& other){ // copy constructor
	//std::cout << "Copy Cons" << std::endl;
	this->inputNodes = std::move(std::unique_ptr<std::vector<InputNeuron>>(new std::vector<InputNeuron>));
	this->hiddenNodes = std::move(std::unique_ptr<std::vector<WorkingNeuron>>(new std::vector<WorkingNeuron>));
	this->outputNodes = std::move(std::unique_ptr<std::vector<WorkingNeuron>>(new std::vector<WorkingNeuron>));
	(*this->inputNodes) = (*other.inputNodes);
	(*this->hiddenNodes) = (*other.hiddenNodes);
	(*this->outputNodes) = (*other.outputNodes);
	this->connectionsHO = other.connectionsHO;
	this->connectionsIH = other.connectionsIH;
}

Network& Network::operator=(Network&& other)noexcept{ //move assignment
	//std::cout << "Move ass 2" << std::endl;
	//std::vector<InputNeuron>().swap(*this->inputNodes);
	//std::vector<WorkingNeuron>().swap(*this->hiddenNodes);
	//std::vector<WorkingNeuron>().swap(*this->outputNodes);
	/*if(this->inputNodes != NULL){
		delete this->inputNodes;
		this->inputNodes = NULL;
	}
	if(this->hiddenNodes != NULL){
		delete this->hiddenNodes;
		this->hiddenNodes = NULL;
	}
	if(this->outputNodes != NULL){
		delete this->outputNodes;
		this->outputNodes = NULL;
	}*/
	this->deleteNodes();

	//this->inputNodes = new std::vector<InputNeuron>();
	//this->hiddenNodes = new std::vector<WorkingNeuron>();
	//this->outputNodes = new std::vector<WorkingNeuron>();

	std::vector<Connection>().swap(connectionsHO);
	std::vector<Connection>().swap(connectionsIH);

	this->inputNodes = std::move(other.inputNodes);
	this->hiddenNodes = std::move(other.hiddenNodes);
	this->outputNodes = std::move(other.outputNodes);
	this->connectionsHO = other.connectionsHO;
	this->connectionsIH = other.connectionsIH;

	other.inputNodes = NULL;
	other.hiddenNodes = NULL;
	other.outputNodes = NULL;
	std::vector<Connection>().swap(other.connectionsIH);
	std::vector<Connection>().swap(other.connectionsHO);

	return *this;
}

Network& Network::operator=(const Network& other){ //copy assignment
	//std::cout << "Copy" << std::endl;
	Network tmp(other);
	(*this) = std::move(tmp);
	return *this;
}

