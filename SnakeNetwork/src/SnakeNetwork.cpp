//============================================================================
// Name        : SnakeNetwork.cpp
// Author      : MarioCake
// Version     :
// Copyright   : Copyright by MarioCake
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <SDL2/SDL.h>
#include <vector>

#include "Game.hpp"

int main(int argc, char *argv[]) {
	//std::cout << "Woops" << std::endl;
	srand(234444);
	Game game(1);
	SDL_Event e;
	bool quit = false;
	while(!quit){
		while(SDL_PollEvent(&e) != 0){
			if(e.type == SDL_QUIT){
				quit = true;
			}else if(e.type == SDL_KEYDOWN){
				switch(e.key.keysym.sym){
				case SDLK_a:
					game.tickRate = 1000;
					game.renderSlowdown = 1;
					break;
				case SDLK_s:
					game.tickRate = 1;
					game.renderSlowdown = 50;
					break;
				case SDLK_d:
					game.tickRate = 1;
					game.renderSlowdown = 1;
					break;
				case SDLK_UP:
					game.renderSlowdown += 10;
					break;
				case SDLK_DOWN:
					game.renderSlowdown -= 10;
					break;
				default:
					break;
				}
			}
		}
		game.update();
		//std::cout << process_size_in_pages() << std::endl;

	}
	/*
	for(int i = 1000; i > 0; i--){
		std::cout << pow((double)i, 2.0)/pow(1000.0, 2.0) << std::endl;
	}*/

	return 0;
}
