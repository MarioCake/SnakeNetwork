/*
 * EasyTexture.hpp
 *
 *  Created on: 23.10.2016
 *      Author: mariocake
 */

#ifndef EASYTEXTURE_HPP_
#define EASYTEXTURE_HPP_

#include <iostream>
#include <unordered_map>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "WindowHandler.hpp"


class EasyTexture {
public:
	EasyTexture();
	EasyTexture(EasyTexture&& other)noexcept; //move
	EasyTexture(const EasyTexture &other); // copy
	EasyTexture& operator=(EasyTexture&& other)noexcept; // move
	EasyTexture& operator=(const EasyTexture &other); // copy
	static std::unordered_map<std::string, SDL_Texture*> textures;
	bool loadFromFile(std::string path);
	void render(int x, int y, SDL_Rect* clip = NULL);
	void free();
	int getWidth() const;
	int getHeight() const;
	virtual ~EasyTexture()noexcept;
	SDL_Texture* texture;
private:
	static std::string basePath;
	int width;
	int height;
};

#endif /* EASYTEXTURE_HPP_ */
