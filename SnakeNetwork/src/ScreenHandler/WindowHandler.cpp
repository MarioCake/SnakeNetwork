/*
 * WindowHandler.cpp
 *
 *  Created on: 20.10.2016
 *      Author: mariocake
 */

#include "WindowHandler.hpp"

SDL_Renderer* Window::renderer;
SDL_Window* Window::window;
SDL_Surface* Window::surface;

bool Window::init(){
	bool success = true;

	Window::window = NULL;
	Window::surface = NULL;
	Window::renderer = NULL;

	if(SDL_Init(SDL_INIT_VIDEO)){
		std::cerr << "Couldn't init Video: " << SDL_GetError() << std::endl;
		success = false;
	}else{
		std::cout << "Init completed" << std::endl;
		this->createWindow();
		if(!Window::window){
			std::cerr << "Couldn't create window: " << SDL_GetError() << std::endl;
			success = false;
		}else {
			std::cout << "Window created" << std::endl;
			this->createScreenSurface();
			if(!Window::surface){
				std::cerr << "Couldn't create Window Surface: " << SDL_GetError() << std::endl;
				success = false;
			}
			std::cout << "Surface created" << std::endl;
			this->createRenderer();
			if(!Window::renderer){
				std::cerr << "Couldn't create Renderer: " << SDL_GetError() << std::endl;
				success = false;
			}else{
				std::cout << "Renderer created" << std::endl;
				SDL_SetRenderDrawColor(Window::renderer, 0xFF, 0xFF, 0xFF, 0xFF);

				int imgFlags = IMG_INIT_PNG;
				if(!(IMG_Init(imgFlags) & imgFlags)){
					std::cerr << "Couldn't init SDL_Image: " << IMG_GetError();
					success = false;
				}
			}
		}
	}

	return success;
}
Window::Window(){
	Window::window = NULL;
	Window::surface = NULL;
	Window::renderer = NULL;
	this->init();
}
Window::~Window(){
	this->close();
}
void Window::close(){

	SDL_DestroyRenderer(Window::renderer);
	SDL_DestroyWindow(Window::window);
	SDL_FreeSurface(Window::surface);
	Window::window = NULL;
	Window::renderer = NULL;
	Window::surface = NULL;

	//TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

void Window::createWindow(){
	Window::window = SDL_CreateWindow("SnakeWork", 0, 0, 1600, 900, SDL_WINDOW_SHOWN);
}
void Window::createScreenSurface(){
	Window::surface = SDL_GetWindowSurface(Window::window);
}
void Window::createRenderer(){
	Window::renderer = SDL_GetRenderer(Window::window);

    if(Window::renderer == NULL){
        Window::renderer = SDL_CreateRenderer(Window::window, -1, SDL_RENDERER_ACCELERATED);
    }
}


