/*
 * WindowHandler.hpp
 *
 *  Created on: 20.10.2016
 *      Author: mariocake
 */

#ifndef WINDOWHANDLER_HPP_
#define WINDOWHANDLER_HPP_

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

class Window{
public:
	static SDL_Window* window;
	static SDL_Surface* surface;
	static SDL_Renderer* renderer;

	Window();
	bool init();
	void close();
	virtual ~Window();
private:
	void createWindow();
	void createScreenSurface();
	void createRenderer();
};


#endif /* WINDOWHANDLER_HPP_ */
