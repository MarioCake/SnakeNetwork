/*
 * TextRenderer.hpp
 *
 *  Created on: 31.10.2016
 *      Author: mariocake
 */

#ifndef TEXTRENDERER_HPP_
#define TEXTRENDERER_HPP_

#include <iostream>
#include <string>
#include <SDL2/SDL_ttf.h>

class TextRenderer {
public:
	TTF_Font* font;
	std::string text;
	void render(int x, int y, std::string text);
	TextRenderer(std::string fontPath);
	TextRenderer();
	virtual ~TextRenderer();
};

#endif /* TEXTRENDERER_HPP_ */
