/*
 * EasyTexture.cpp
 *
 *  Created on: 23.10.2016
 *      Author: mariocake
 */

#include "EasyTexture.hpp"

std::unordered_map<std::string, SDL_Texture*> EasyTexture::textures = {};
std::string EasyTexture::basePath = SDL_GetBasePath();

EasyTexture::EasyTexture() {
	this->width = 0;
	this->height = 0;
	this->texture = nullptr;
}

EasyTexture::EasyTexture(EasyTexture&& other)noexcept{ // move
	this->texture = other.texture;
	this->width = other.width;
	this->height = other.height;
	other.texture = nullptr;
}

EasyTexture::EasyTexture(const EasyTexture& other){ // copy
	this->texture = other.texture;
	this->width = other.width;
	this->height = other.height;
}

EasyTexture& EasyTexture::operator=(EasyTexture&& other)noexcept{ //move
	this->texture = other.texture;
	this->width = other.width;
	this->height = other.height;
	other.texture = nullptr;
	return *this;
}

EasyTexture& EasyTexture::operator=(const EasyTexture& other){ // copy
	EasyTexture tmp(other);
	(*this) = std::move(tmp);
	return *this;
}

EasyTexture::~EasyTexture() noexcept{
	this->free();
}
void EasyTexture::free(){
	if(this->texture != nullptr){
		SDL_DestroyTexture(this->texture);
		this->texture = nullptr;
		width = 0;
		height = 0;
	}
}

void EasyTexture::render(int x, int y, SDL_Rect* clip){
	SDL_Rect renderQuad = {x, y, this->width, this->height};
	if(clip != nullptr){
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	SDL_RenderCopy(Window::renderer, this->texture, clip, &renderQuad);
}

bool EasyTexture::loadFromFile(std::string path){

	std::unordered_map<std::string, SDL_Texture*>::const_iterator got = textures.find(path);

	//std::cout << textures.size() << std::endl;

	if(got == textures.end()){
		this->free();

		path = EasyTexture::basePath+path;
		SDL_Surface* loadedSurface = IMG_Load(path.c_str());
		if(loadedSurface == nullptr){
			std::cerr << "Couldn't load image: " << path.c_str() << ". " << IMG_GetError() << std::endl;
		}else {
			SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));

			this->texture = SDL_CreateTextureFromSurface(Window::renderer, loadedSurface);
			if(this->texture == nullptr){
				std::cerr << "Couldn't create Texture: " << path.c_str() << ". " << SDL_GetError() << std::endl;
			}else{
				this->width = loadedSurface->w;
				this->height = loadedSurface->h;
				textures[path] = this->texture;
			}
			SDL_FreeSurface(loadedSurface);
		}
	}else{
		this->free();
		this->texture = got->second;
	}
	//newTexture = NULL;
	return this->texture != nullptr;
}
int EasyTexture::getWidth() const{
	return this->width;
}
int EasyTexture::getHeight() const{
	return this->height;
}
